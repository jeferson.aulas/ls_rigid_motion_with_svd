/* ----------------------------------------------------------------------------
 * Copyright 2021, Jeferson Lima
 * All Rights Reserved
 * See LICENSE for the license information
 * -------------------------------------------------------------------------- */

/**
 *  @file   ls_transformation.hpp
 *  @author Jeferson Lima
 *  @brief  Header file for Least Squares for Point Cloud Homogeneous Transformation
 *  @date   May 14, 2021
 **/

#ifndef LS_TRANSFORMATION_HPP

#include <iostream>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/filters/random_sample.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <Eigen/Dense>

class LeastSquareT{

  public:

    pcl::PointCloud<pcl::PointXYZ>::Ptr _transformed_cloud;

    // constructor
    LeastSquareT(const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in, int iter);

    LeastSquareT(const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in,
                 const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out, int iter);
    //destructor
    ~LeastSquareT();

  private:

    void applyTandNoise(const Eigen::Affine3f& Hin);

    void opSolve();

    void convertMatrix(const pcl::PointCloud<pcl::PointXYZ>::Ptr c_in, const Eigen::MatrixXd& Mat);

    void addRandomSample(const pcl::PointCloud<pcl::PointXYZ>::Ptr c_in, unsigned int n_samples);
    

    // variables
    pcl::PointCloud<pcl::PointXYZ>::Ptr _cloud;
    pcl::registration::TransformationEstimationSVD<pcl::PointXYZ,pcl::PointXYZ>::Matrix4 _H;
    int _iter;


};

#endif
