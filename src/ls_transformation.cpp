/* ----------------------------------------------------------------------------
 * Copyright 2021, Jeferson Lima
 * All Rights Reserved
 * See LICENSE for the license information
 * -------------------------------------------------------------------------- */

/**
 *  @file   ls_transformation.cpp
 *  @author Jeferson Lima
 *  @brief  Source file for Least Squares for Point Cloud Homogeneous Transformation
 *  @date   May 14, 2021
 **/

#include "ls_transformation.hpp"

LeastSquareT::LeastSquareT(
                        const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in,
                        int iter
                        ) : _cloud(cloud_in), _iter(iter)
{
  Eigen::Affine3f H = Eigen::Affine3f::Identity();
  H.translation() << 2.5, 4.0, -6.0;
  //phi
  H.rotate(Eigen::AngleAxisf (0.4, Eigen::Vector3f::UnitZ()));
  applyTandNoise(H);
  opSolve();
}

LeastSquareT::~LeastSquareT() {}


LeastSquareT::LeastSquareT(
                        const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in,
                        const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out, int iter
                        ) : _cloud(cloud_in), _transformed_cloud(cloud_out), _iter(iter)
{
  opSolve();
}


void LeastSquareT::convertMatrix(const pcl::PointCloud<pcl::PointXYZ>::Ptr in, const Eigen::MatrixXd& Mat)
{

}

void LeastSquareT::addRandomSample(const pcl::PointCloud<pcl::PointXYZ>::Ptr c_in,
                     unsigned int n_samples)
{
  for (int i = 0; i < n_samples; i++)
  {
    c_in->push_back(pcl::PointXYZ (rand (), rand (), rand ()));
    //TODO rand by max min pclouds
  }
}


 void LeastSquareT::applyTandNoise(const Eigen::Affine3f& Hin)
 {
  pcl::PointCloud<pcl::PointXYZ>::Ptr t_cloud (new pcl::PointCloud<pcl::PointXYZ> ());

  pcl::transformPointCloud(*_cloud, *t_cloud, Hin);

  for (auto& point : *t_cloud)
  {
    point.x = point.x + point.x*0.05*(1024 * rand() / (RAND_MAX + 1.0f));
    point.y = point.y + point.y*0.05*(1024 * rand() / (RAND_MAX + 1.0f));
    point.z = point.z + point.z*0.05*(1024 * rand() / (RAND_MAX + 1.0f));
  }
  _transformed_cloud = t_cloud;
}

void LeastSquareT::opSolve()
{
  pcl::registration::TransformationEstimationSVD<pcl::PointXYZ,pcl::PointXYZ>::Matrix4 Hout;
  pcl::registration::TransformationEstimationSVD<pcl::PointXYZ,pcl::PointXYZ> SVD;
  pcl::PointCloud<pcl::PointXYZ>::Ptr t_cloud (new pcl::PointCloud<pcl::PointXYZ> ());

  Eigen::Matrix3f angles;

  _H.setIdentity(4,4);

  for(int i = 0; i < _iter; i++)
  {
    std::cout << "Iteration: " << i << std::endl; 

    SVD.estimateRigidTransformation(*_cloud,*_transformed_cloud,Hout);

    pcl::transformPointCloud(*_cloud, *t_cloud, Hout);
    *_cloud = *t_cloud;
    _H = _H * Hout;
    std::cout << _H.matrix() << std::endl;

    angles = Hout.block(0,0,3,3);
    std::cout << "Angles" << std::endl;
    std::cout << angles.eulerAngles(0, 1, 2) << std::endl;

  }
  std::cout << "Final H: " << std::endl;
  std::cout << _H.matrix() << std::endl;

  angles = _H.block(0,0,3,3);
  std::cout << "Final angles" << std::endl;
  std::cout << angles.eulerAngles(0, 1, 2) << std::endl;
}
